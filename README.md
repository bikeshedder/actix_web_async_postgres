# Example of actix with different connection pool implementations and database libraries.

Currently this repository contains three implementations:

1. L3-37 (`l337`) with `tokio-postgres`
2. `bb8` with `tokio-postgres`
3. `r2d2` with `postgres`

## Installation and usage

    $ createdb mydb
    $ psql mydb < database.sql
    $ cp .env.example .env # edit this file afterwards
    $ cargo build
    $ ./target/debug/actix_web_async_postgres

If everything goes well you should now be able to access three URLs which all execute the same query and return a JSON array of objects.

- http://localhost:8000/v1.0/event\_list\_l337
- http://localhost:8000/v1.0/event\_list\_bb8
- http://localhost:8000/v1.0/event\_list\_r2d2

## Benchmark

This repository also contains a benchmark script comparing the three implementations. The benchmark uses [wrk](https://github.com/wg/wrk) with 4 threads and 128 concurrent connections.

Running this benchmark on a DELL XPS 13 with i7-8550U CPU @ 1.80GHz, 16 GiB of RAM and a SSD using Fedora 29 with PostgreSQL 10.8 yields the following results:`

### L3-37 + tokio-postgres

```
Running 5m test @ http://localhost:8000/v1.0/event_list_l337
  4 threads and 128 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.89ms    1.00ms  31.75ms   78.91%
    Req/Sec     5.45k   347.45     8.95k    80.10%
  Latency Distribution
     50%    5.71ms
     75%    6.31ms
     90%    7.03ms
     99%    9.42ms
  6508822 requests in 5.00m, 2.61GB read
Requests/sec:  21691.51
Transfer/sec:      8.90MB
```

### bb8 + tokio-postgres

```
Running 5m test @ http://localhost:8000/v1.0/event_list_bb8
  4 threads and 128 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.80ms  792.17us  26.06ms   81.07%
    Req/Sec     4.73k   251.61     6.48k    80.90%
  Latency Distribution
     50%    6.62ms
     75%    7.08ms
     90%    7.70ms
     99%    9.67ms
  5647216 requests in 5.00m, 2.26GB read
Requests/sec:  18821.11
Transfer/sec:      7.72MB
```

### r2d2 + postgres

```
Running 5m test @ http://localhost:8000/v1.0/event_list_r2d2
  4 threads and 128 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     8.60ms    4.13ms  71.29ms   80.42%
    Req/Sec     3.83k   212.12     5.21k    74.70%
  Latency Distribution
     50%    7.43ms
     75%   10.10ms
     90%   13.85ms
     99%   23.50ms
  4575370 requests in 5.00m, 1.83GB read
Requests/sec:  15249.25
Transfer/sec:      6.25MB
```

All three implementations solve the [C10k problem](https://en.wikipedia.org/wiki/C10k_problem) with ease.
