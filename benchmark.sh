#!/bin/bash

set -e

WRK_CMD=wrk
URLS=(
    "http://localhost:8000/v1.0/event_list_l337"
    "http://localhost:8000/v1.0/event_list_bb8"
    "http://localhost:8000/v1.0/event_list_r2d2"
)

echo "Warming up..."
for URL in ${URLS[@]}; do
    $WRK_CMD -t 4 -c 128 -d 10 $URL > /dev/null
done

echo "Running benchmarks..."
for URL in ${URLS[@]}; do
    $WRK_CMD -t 4 -c 128 -d 300 --latency $URL
done
