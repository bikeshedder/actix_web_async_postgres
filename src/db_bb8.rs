use actix_web::Error as AWError;
use actix_web::error::ErrorInternalServerError;
use bb8;
use bb8_postgres;
use futures::{Future, Stream, future::lazy};
use tokio_postgres;

use crate::api::Event;
use crate::db_common::PoolConfig;

pub type ConnectionManager = bb8_postgres::PostgresConnectionManager<tokio_postgres::NoTls>;
pub type Pool = bb8::Pool<ConnectionManager>;
pub type Client = tokio_postgres::Client;

pub fn create_pool(
    runner: &mut actix_rt::SystemRunner,
    pg_config: &tokio_postgres::Config,
    pool_config: &PoolConfig,
) -> Pool {
    let bb8_manager = ConnectionManager::new(
        pg_config.clone(),
        tokio_postgres::NoTls
    );
    runner.block_on(lazy(|| {
        Pool::builder()
        .min_idle(Some(pool_config.min_size.into()))
        .max_size(pool_config.max_size.into())
        .build(bb8_manager)
        .map_err(|e| bb8::RunError::User(e))
        .map_err(|e| panic!("{:?}", e))
    })).unwrap()
}

pub fn event_list(
    pool: &Pool,
) -> impl Future<Item = Vec<Event>, Error = AWError> {
    pool.run(move |mut client: Client| {
        client
            .prepare("SELECT id, name FROM event")
            .then(move |res| match res {
                Ok(stmt) => Ok((stmt, client)),
                Err(e) => Err((e, client))
            })
            .and_then(move |(stmt, mut client)| {
                client.query(&stmt, &[])
                    .collect()
                    .then(move |res| match res {
                        Ok(rows) => Ok((rows.iter().map(|row|
                            Event {
                                id: row.get(0),
                                name: row.get(1),
                            }
                        ).collect(), client)),
                        Err(e) => Err((e, client)),
                    })
            })
    })
    .map_err(|e| ErrorInternalServerError(e))
}
