use std::env;
use std::path::Path;

pub use tokio_postgres::config::Config as PgConfig;


pub fn pg_config_from_env() -> Result<PgConfig, String> {
    let mut config = PgConfig::new();
    if let Ok(host) = env::var("PG_HOST") {
        config.host(host.as_str());
    } else {
        if Path::new("/run/postgresql").exists() {
            config.host("/run/postgresql");
        } else {
            config.host("/tmp");
        }
    }
    if let Ok(port_string) = env::var("PG_PORT") {
        let port = port_string.parse::<u16>()
            .map_err(|_| format!("Invalid PG_PORT: {}", port_string))?;
        config.port(port);
    }
    if let Ok(user) = env::var("PG_USER") {
        config.user(user.as_str());
    } else if let Ok(user) = env::var("USER") {
        config.user(user.as_str());
    } else {
        return Err("Missing PG_USER. Fallback to USER failed as well.".into());
    }
    if let Ok(password) = env::var("PG_PASSWORD") {
        config.password(password.as_str());
    }
    if let Ok(dbname) = env::var("PG_DBNAME") {
        config.dbname(dbname.as_str());
    }
    Ok(config)
}

#[derive(Debug)]
pub struct PoolConfig {
    pub min_size: u16,
    pub max_size: u16,
}

impl PoolConfig {
    pub fn from_env() -> Result<PoolConfig, String> {
        let mut config = PoolConfig { min_size: 1, max_size: 16 };
        if let Ok(min_size_string) = env::var("POOL_MIN_SIZE") {
            config.min_size = min_size_string.parse::<u16>()
                .map_err(|_| format!("Invalid POOL_MIN_SIZE: {}", min_size_string))?;
        }
        if let Ok(max_size_string) = env::var("POOL_MAX_SIZE") {
            config.max_size = max_size_string.parse::<u16>()
                .map_err(|_| format!("Invalid POOL_MIN_SIZE: {}", max_size_string))?;
        }
        Ok(config)
    }
}
