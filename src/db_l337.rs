use actix_rt;
use actix_web::Error as AWError;
use actix_web::error::ErrorInternalServerError;
use futures::{Future, Stream};
use tokio_postgres;

use crate::api::Event;
use crate::db_common::PoolConfig;

pub type ConnectionManager = l337_postgres::PostgresConnectionManager<tokio_postgres::NoTls>;
pub type Pool = l337::Pool<ConnectionManager>;

pub fn create_pool(
    runner: &mut actix_rt::SystemRunner,
    pg_config: &tokio_postgres::Config,
    config: &PoolConfig
) -> Pool {
    let l337_config = l337::Config {
        min_size: config.min_size.into(),
        max_size: config.max_size.into(),
    };
    let l337_mgr = ConnectionManager::new(
        pg_config.clone(),
        tokio_postgres::NoTls
    );
    runner
        .block_on(l337::Pool::new(l337_mgr, l337_config))
        .expect("db connection error (l337 pool)")
}

pub fn event_list(
    pool: &Pool,
) -> impl Future<Item = Vec<Event>, Error = AWError> {
    pool.connection()
        .and_then(|mut conn| {
            conn.client
                .prepare("SELECT id, name FROM event")
                .and_then(move |stmt| {
                    let query: tokio_postgres::impls::Query = conn.client.query(&stmt, &[]);
                    query
                        .map(|row| {
                            Event {
                                id: row.get(0),
                                name: row.get(1),
                            }
                        }).collect()
                })
                .map_err(|e| l337::Error::External(e))
        })
        // XXX tokio_postrges::error::Error does not implement the Display trait
        //.map_err(|e| ErrorInternalServerError(e))
        .map_err(|_| ErrorInternalServerError("pool.connection() failed"))
}
