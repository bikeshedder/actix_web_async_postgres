use actix_web::{web, Error as AWError};
use failure::Error;
use futures::Future;
use uuid::Uuid;

use crate::db_common::PoolConfig;
use crate::api::Event;

pub type ConnectionManager = r2d2_postgres::PostgresConnectionManager<postgres::NoTls>;
pub type Pool = r2d2::Pool<ConnectionManager>;

pub fn create_pool(
    pg_config: &tokio_postgres::Config,
    pool_config: &PoolConfig
) -> Pool {
    let r2d2_manager = ConnectionManager::new(
        postgres::Config::from(pg_config.clone()),
        postgres::NoTls,
    );
    Pool::builder()
        .min_idle(Some(pool_config.min_size.into()))
        .max_size(pool_config.max_size.into())
        .build(r2d2_manager)
        .expect("db connection error (r2d2 pool)")
}

pub fn event_list(
    pool: &Pool,
) -> impl Future<Item = Vec<Event>, Error = AWError> {
    let pool = pool.clone();
    web::block::<_, _, Error>(move || {
        let mut conn = pool.get()?;
        let rows = conn.query("SELECT id::text, name FROM event", &[])?;
        let events = rows.iter().map(|row| {
            let id: String = row.get(0);
            let uuid = Uuid::parse_str(id.as_str()).expect("Uuid::parse_str failed");
            Event {
                id: uuid,
                name: row.get(1)
            }
        }).collect();
        Ok(events)
    })
    .from_err()
}
