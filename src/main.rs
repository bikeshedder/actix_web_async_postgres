use std::io;

use actix_web::{web, App, HttpResponse, HttpServer, Responder, Error};
use dotenv::dotenv;
use futures::Future;
extern crate serde_derive;

mod api;
mod db_common;
use db_common::{pg_config_from_env, PoolConfig};
mod db_l337;
mod db_r2d2;
mod db_bb8;

fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

fn event_list_l337(
    db: web::Data<db_l337::Pool>
) -> impl Future<Item = HttpResponse, Error = Error> {
    db_l337::event_list(&db)
        .from_err()
        .and_then(|events| Ok(HttpResponse::Ok().json(events)))
}

fn event_list_r2d2(
    db: web::Data<db_r2d2::Pool>
) -> impl Future<Item = HttpResponse, Error = Error> {
    db_r2d2::event_list(&db)
        .from_err()
        .and_then(|events| Ok(HttpResponse::Ok().json(events)))
}

fn event_list_bb8(
    db: web::Data<db_bb8::Pool>
) -> impl Future<Item = HttpResponse, Error = Error> {
    db_bb8::event_list(&db)
        .from_err()
        .and_then(|events| Ok(HttpResponse::Ok().json(events)))
}

fn main() -> io::Result<()> {
    dotenv().ok();

    let pg_config = pg_config_from_env().expect("Invalid PG config");
    let pool_config = PoolConfig::from_env().expect("Invalid pool config");
    let mut sys = actix_rt::System::new("actix_example");

    println!("Creating L3-37 pool...");
    let l337_pool = db_l337::create_pool(&mut sys, &pg_config, &pool_config);
    println!("Creating bb8 pool...");
    let bb8_pool = db_bb8::create_pool(&mut sys, &pg_config, &pool_config);
    println!("Creating r2d2 pool...");
    let r2d2_pool = db_r2d2::create_pool(&pg_config, &pool_config);
    println!("All pools ready.");
    println!("");

    HttpServer::new(move || {
        App::new()
            .data(l337_pool.clone())
            .data(bb8_pool.clone())
            .data(r2d2_pool.clone())
            .route("/", web::get().to(index))
            .route("/v1.0/event_list_l337", web::get().to_async(event_list_l337))
            .route("/v1.0/event_list_bb8", web::get().to_async(event_list_bb8))
            .route("/v1.0/event_list_r2d2", web::get().to_async(event_list_r2d2))
    })
    .bind("127.0.0.1:8000")?
    .start();

    println!("Server running at http://localhost:8000");
    println!("");
    println!("Try the following URLs:");
    println!("- http://localhost:8000/v1.0/event_list_l337");
    println!("- http://localhost:8000/v1.0/event_list_r2d2");
    println!("- http://localhost:8000/v1.0/event_list_bb8");

    sys.run()
}

